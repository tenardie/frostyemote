package oyunbaz.frostyemote;

import oyunbaz.frostyemote.eventhandlers.AsyncPlayerChatEventHandler;
import oyunbaz.frostyemote.eventhandlers.PlayerInteractAtEntityEventHandler;
import oyunbaz.frostyemote.eventhandlers.PlayerJoinEventHandler;
import org.bukkit.plugin.PluginManager;

public class EventRegistry {

    private static EventRegistry instance;

    private EventRegistry() {

    }

    public static EventRegistry getInstance() {
        if (instance == null) {
            instance = new EventRegistry();
        }
        return instance;
    }

    public void registerEvents() {

        FrostyEmote mainInstance = FrostyEmote.getInstance();
        PluginManager manager = mainInstance.getServer().getPluginManager();

        manager.registerEvents(new AsyncPlayerChatEventHandler(), mainInstance);
        manager.registerEvents(new PlayerInteractAtEntityEventHandler(), mainInstance);
        manager.registerEvents(new PlayerJoinEventHandler(), mainInstance);

    }

}
