package oyunbaz.frostyemote.eventhandlers;

import org.bukkit.entity.Player;
import oyunbaz.frostyemote.data.PersistentData;
import oyunbaz.frostyemote.objects.CharacterCard;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static oyunbaz.frostyemote.commands.LocalChatCommand.addPlayerToLocalChat;

public class PlayerJoinEventHandler implements Listener {

    @EventHandler()
    public void handle(PlayerJoinEvent event) {
        if (!PersistentData.getInstance().hasCard(event.getPlayer().getUniqueId())) {
            CharacterCard newCard = new CharacterCard(event.getPlayer().getUniqueId());
            PersistentData.getInstance().getCards().add(newCard);
        }
        Player player =  event.getPlayer();
        addPlayerToLocalChat(player);
    }

}
