package oyunbaz.frostyemote.eventhandlers;

import oyunbaz.frostyemote.FrostyEmote;
import oyunbaz.frostyemote.data.EphemeralData;
import oyunbaz.frostyemote.data.PersistentData;
import oyunbaz.frostyemote.objects.CharacterCard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

public class PlayerInteractAtEntityEventHandler implements Listener {

    @EventHandler()
    public void handle(PlayerInteractAtEntityEvent event) {
        if (event.getRightClicked() instanceof Player) {

            Player target = (Player) event.getRightClicked();
            CharacterCard card = PersistentData.getInstance().getCard(target.getUniqueId());

            Player player = event.getPlayer();

            if (!FrostyEmote.getInstance().getConfig().getBoolean("rightClickToViewCard")) {
                return;
            }

            if (!EphemeralData.getInstance().getPlayersWithRightClickCooldown().contains(player.getUniqueId())) {
                EphemeralData.getInstance().getPlayersWithRightClickCooldown().add(player.getUniqueId());

                if (player.hasPermission("fe.card.show.others") || player.hasPermission("fe.card.*") || player.hasPermission("rp.default")) {

                    player.sendMessage(ChatColor.BOLD + "" + ChatColor.AQUA + "\n == " + "Kimlik Kartı == " + Bukkit.getOfflinePlayer(card.getPlayerUUID()).getName() + " == ");
                    player.sendMessage(ChatColor.AQUA + "Isim: " + card.getName());
                    player.sendMessage(ChatColor.AQUA + "Irk: " + card.getRace());
                    player.sendMessage(ChatColor.AQUA + "Kultur: " + card.getSubculture());
                    player.sendMessage(ChatColor.AQUA + "Yas: " + card.getAge());
                    player.sendMessage(ChatColor.AQUA + "Cinsiyet: " + card.getGender());
                    player.sendMessage(ChatColor.AQUA + "Din: " + card.getReligion());

                    int seconds = 2;
                    FrostyEmote.getInstance().getServer().getScheduler().runTaskLater(FrostyEmote.getInstance(), new Runnable() {
                        @Override
                        public void run() {
                            EphemeralData.getInstance().getPlayersWithRightClickCooldown().remove(player.getUniqueId());

                        }
                    }, seconds * 20);
                }

            }

        }
    }

}
