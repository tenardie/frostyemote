package oyunbaz.frostyemote.frostymedic;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import oyunbaz.frostyemote.FrostyEmote;
import oyunbaz.frostyemote.Messenger;
import oyunbaz.frostyemote.utils.ColorChecker;

import java.util.ArrayList;
import java.util.HashMap;

public class DeathEvent implements Listener {
    public ArrayList<String> remove = new ArrayList<String>();
    HashMap<String, Float> saturation = new HashMap<String, Float>();
    HashMap<String, Integer> foodlevel = new HashMap<String, Integer>();

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }
        Player player = (Player) event.getEntity();
        String playerName = player.getPlayerListName();
        double damage = event.getDamage();
        if (player.getGameMode() == GameMode.CREATIVE || player.getGameMode() == GameMode.SPECTATOR) {
            return;
        }
        if (damage < player.getHealth()) {
            return;
        }
        if (FrostyEmote.injured.contains(player.getUniqueId())) {
            FrostyEmote.injured.remove(player.getUniqueId());
            if (!(player.getKiller() instanceof Player)) {
                return;
            }
            player.getKiller().sendMessage(ChatColor.DARK_RED + playerName + "isimli kişiyi öldürdün!");
            return;
        }
        if (player.getKiller() != null) {
            player.getKiller().sendMessage(ChatColor.DARK_RED + playerName + "isimli kişiyi bayılttın!");
        }
        Player KillerPlayer = player.getKiller();
        String KillerName = KillerPlayer.getName();
        player.setHealth(20);
        player.sendMessage(ChatColor.RED + KillerName + "isimli kişi tarafından bayıltıldın!");
        int emoteRadius = FrostyEmote.getInstance().getConfig().getInt("emoteRadius");
        String emoteColor = FrostyEmote.getInstance().getConfig().getString("emoteColor");
        String playerColor = FrostyEmote.getInstance().getConfig().getString("playerColor");
        Messenger.getInstance().sendRPMessageToPlayersWithinDistance(player, ColorChecker.getInstance().getColorByName(playerColor) + "" + KillerName + " " + ColorChecker.getInstance().getColorByName(emoteColor) + "" + ChatColor.ITALIC + ", " + playerName + " isimli kişiyi bayılttı!", emoteRadius);
        FrostyEmote.injured.add(player.getUniqueId());
        player.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 10000000, 0));
        event.setCancelled(true);
        saturation.put(player.getUniqueId().toString(), player.getSaturation());
        foodlevel.put(player.getUniqueId().toString(), player.getFoodLevel());
        player.setFoodLevel(1);
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        Player player = event.getPlayer();
        Entity entity = event.getRightClicked();
        if (entity instanceof Player) {
            if (FrostyEmote.injured.contains(entity.getUniqueId()) && event.getPlayer().getItemInHand() != null && event.getPlayer().getItemInHand().getType().equals(Material.SLIME_BALL)) {
                    Player injure = (Player) event.getRightClicked();
                    injure.setFoodLevel(injure.getFoodLevel() + 1);
                    if (injure.getFoodLevel() == 20) {
                        for (PotionEffect pe : injure.getActivePotionEffects()) {
                            injure.removePotionEffect(pe.getType());
                        }
                        injure.setHealth(10);
                        injure.sendMessage(ChatColor.GREEN + player.getName() + " isimli kişi sana ilk yardım yaptı.");
                        player.sendMessage(ChatColor.GREEN + injure.getName() + " isimli kişiye ilk yardım yaptın.");
                        int emoteRadius = FrostyEmote.getInstance().getConfig().getInt("emoteRadius");
                        String emoteColor = FrostyEmote.getInstance().getConfig().getString("emoteColor");
                        String playerColor = FrostyEmote.getInstance().getConfig().getString("playerColor");
                        Messenger.getInstance().sendRPMessageToPlayersWithinDistance(player, ColorChecker.getInstance().getColorByName(playerColor) + "" + player.getName() + " " + ColorChecker.getInstance().getColorByName(emoteColor) + "" + ChatColor.ITALIC + ", " + injure.getName() + " isimli kişiye ilk yardım yaptı!", emoteRadius);
                        FrostyEmote.injured.remove(injure.getUniqueId());
                        injure.setFoodLevel(foodlevel.get(injure.getUniqueId().toString()));
                        injure.setSaturation(saturation.get(injure.getUniqueId().toString()));
                        player.getInventory().getItemInMainHand().setAmount
                                (player.getInventory().getItemInMainHand().getAmount() - 1);
                    }
                }
            }
        }


    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if (FrostyEmote.injured.contains(player.getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        if (FrostyEmote.injured.contains(player.getUniqueId())) {
            player.sendMessage(ChatColor.RED + "Yaralıyken komut kullanamazsın.");
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onTeleport(PlayerTeleportEvent event) {
        Player player = event.getPlayer();
        if (FrostyEmote.injured.contains(player.getUniqueId())) {
            player.sendMessage(ChatColor.RED + "Yaralıyken Işınlanamazsın.");
            event.setCancelled(true);
        }
    }
}
