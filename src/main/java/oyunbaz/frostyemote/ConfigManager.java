package oyunbaz.frostyemote;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ConfigManager {

    private static ConfigManager instance;

    private boolean altered = false;

    private ConfigManager() {

    }

    public static ConfigManager getInstance() {
        if (instance == null) {
            instance = new ConfigManager();
        }
        return instance;
    }

    public void handleVersionMismatch() {
        // set version
        if (!FrostyEmote.getInstance().getConfig().isString("version")) {
            FrostyEmote.getInstance().getConfig().addDefault("version", FrostyEmote.getInstance().getVersion());
        }
        else {
            FrostyEmote.getInstance().getConfig().set("version", FrostyEmote.getInstance().getVersion());
        }

        // add defaults if they don't exist
        if (!FrostyEmote.getInstance().getConfig().isInt("localChatRadius")) {
            FrostyEmote.getInstance().getConfig().addDefault("localChatRadius", 25);
        }

        if (!FrostyEmote.getInstance().getConfig().isInt("whisperChatRadius")) {
            FrostyEmote.getInstance().getConfig().addDefault("whisperChatRadius", 3);
        }

        if (!FrostyEmote.getInstance().getConfig().isInt("yellChatRadius")) {
            FrostyEmote.getInstance().getConfig().addDefault("yellChatRadius", 50);
        }

        if (!FrostyEmote.getInstance().getConfig().isInt("emoteRadius")) {
            FrostyEmote.getInstance().getConfig().addDefault("emoteRadius", 25);
        }
        if (!FrostyEmote.getInstance().getConfig().isInt("doRadius")) {
            FrostyEmote.getInstance().getConfig().addDefault("doRadius", 25);
        }

        if (!FrostyEmote.getInstance().getConfig().isInt("changeNameCooldown")) {
            FrostyEmote.getInstance().getConfig().addDefault("changeNameCooldown", 2147483647);
        }

        if (!FrostyEmote.getInstance().getConfig().isString("localChatColor")) {
            FrostyEmote.getInstance().getConfig().addDefault("localChatColor", "gray");
        }

        if (!FrostyEmote.getInstance().getConfig().isString("whisperChatColor")) {
            FrostyEmote.getInstance().getConfig().addDefault("whisperChatColor", "white");
        }

        if (!FrostyEmote.getInstance().getConfig().isString("yellChatColor")) {
            FrostyEmote.getInstance().getConfig().addDefault("yellChatColor", "red");
        }

        if (!FrostyEmote.getInstance().getConfig().isString("emoteColor")) {
            FrostyEmote.getInstance().getConfig().addDefault("emoteColor", "gray");
        }
        if (!FrostyEmote.getInstance().getConfig().isString("doColor")) {
            FrostyEmote.getInstance().getConfig().addDefault("doColor", "white");
        }

        if (!FrostyEmote.getInstance().getConfig().isBoolean("rightClickToViewCard")) {
            FrostyEmote.getInstance().getConfig().addDefault("rightClickToViewCard", false);
        }

        if (!FrostyEmote.getInstance().getConfig().isInt("localOOCChatRadius")) {
            FrostyEmote.getInstance().getConfig().addDefault("localOOCChatRadius", 25);
        }
        if (!FrostyEmote.getInstance().getConfig().isString("playerColor")) {
            FrostyEmote.getInstance().getConfig().addDefault("playerColor", "gray");
        }

        if (!FrostyEmote.getInstance().getConfig().isString("localOOCChatColor")) {
            FrostyEmote.getInstance().getConfig().addDefault("localOOCChatColor", "gray");
        }

        deleteOldConfigOptionsIfPresent();

        FrostyEmote.getInstance().getConfig().options().copyDefaults(true);
        FrostyEmote.getInstance().saveConfig();
    }

    private void deleteOldConfigOptionsIfPresent() {

        if (FrostyEmote.getInstance().getConfig().isInt("test")) {
            FrostyEmote.getInstance().getConfig().set("test", null);
        }

    }

    public void setConfigOption(String option, String value, Player player) {

        if (FrostyEmote.getInstance().getConfig().isSet(option)) {

            if (option.equalsIgnoreCase("version")) {
                player.sendMessage(ChatColor.RED + "Versiyon eklenemedi!");
                return;
            }
            else if (option.equalsIgnoreCase("localChatRadius") ||
                    option.equalsIgnoreCase("whisperChatRadius") ||
                    option.equalsIgnoreCase("yellChatRadius") ||
                    option.equalsIgnoreCase("changeNameCooldown") ||
                    option.equalsIgnoreCase("emoteRadius") ||
                    option.equalsIgnoreCase("doRadius") ||
                    option.equalsIgnoreCase("localOOCChatRadius")) {
                FrostyEmote.getInstance().getConfig().set(option, Integer.parseInt(value));
                player.sendMessage(ChatColor.GREEN + "Ayarlandi!");
            }
            else if (option.equalsIgnoreCase("rightClickToViewCard")) {
                FrostyEmote.getInstance().getConfig().set(option, Boolean.parseBoolean(value));
                player.sendMessage(ChatColor.GREEN + "Ayarlandi!");
            }
            else if (option.equalsIgnoreCase("doubletest")) {
                FrostyEmote.getInstance().getConfig().set(option, Double.parseDouble(value));
                player.sendMessage(ChatColor.GREEN + "Ayarlandi!");
            }
            else {
                FrostyEmote.getInstance().getConfig().set(option, value);
                player.sendMessage(ChatColor.GREEN + "Ayarlandi!");
            }

            // save
            FrostyEmote.getInstance().saveConfig();
            altered = true;
        }
        else {
            player.sendMessage(ChatColor.RED + String.format("'%s' degeri bulunamadi.", option));
        }

    }

    public void saveConfigDefaults() {
        FrostyEmote.getInstance().getConfig().addDefault("version", FrostyEmote.getInstance().getVersion());
        FrostyEmote.getInstance().getConfig().addDefault("localChatRadius", 25);
        FrostyEmote.getInstance().getConfig().addDefault("whisperChatRadius", 2);
        FrostyEmote.getInstance().getConfig().addDefault("yellChatRadius", 50);
        FrostyEmote.getInstance().getConfig().addDefault("emoteRadius", 25);
        FrostyEmote.getInstance().getConfig().addDefault("doRadius", 25);
        FrostyEmote.getInstance().getConfig().addDefault("changeNameCooldown", 2147483647);
        FrostyEmote.getInstance().getConfig().addDefault("localChatColor", "gray");
        FrostyEmote.getInstance().getConfig().addDefault("whisperChatColor", "white");
        FrostyEmote.getInstance().getConfig().addDefault("yellChatColor", "red");
        FrostyEmote.getInstance().getConfig().addDefault("emoteColor", "gray");
        FrostyEmote.getInstance().getConfig().addDefault("doColor", "white");
        FrostyEmote.getInstance().getConfig().addDefault("rightClickToViewCard", false);
        FrostyEmote.getInstance().getConfig().addDefault("localOOCChatRadius", 25);
        FrostyEmote.getInstance().getConfig().addDefault("localOOCChatColor", "gray");
        FrostyEmote.getInstance().getConfig().addDefault("playerColor", "gray");
        FrostyEmote.getInstance().getConfig().options().copyDefaults(true);
        FrostyEmote.getInstance().saveConfig();
    }

    public void sendPlayerConfigList(Player player) {
        player.sendMessage(ChatColor.AQUA + "version: " + FrostyEmote.getInstance().getConfig().getString("version")
                + ", localChatRadius: " + FrostyEmote.getInstance().getConfig().getInt("localChatRadius")
                + ", whisperChatRadius: " + FrostyEmote.getInstance().getConfig().getInt("whisperChatRadius")
                + ", yellChatRadius: " + FrostyEmote.getInstance().getConfig().getInt("yellChatRadius")
                + ", emoteRadius: " + FrostyEmote.getInstance().getConfig().getInt("emoteRadius")
                + ", doRadius: " + FrostyEmote.getInstance().getConfig().getInt("doRadius")
                + ", changeNameCooldown: " + FrostyEmote.getInstance().getConfig().getInt("changeNameCooldown")
                + ", localChatColor: " + FrostyEmote.getInstance().getConfig().getString("localChatColor")
                + ", whisperChatColor: " + FrostyEmote.getInstance().getConfig().getString("whisperChatColor")
                + ", yellChatColor: " + FrostyEmote.getInstance().getConfig().getString("yellChatColor")
                + ", emoteColor: " + FrostyEmote.getInstance().getConfig().getString("emoteColor")
                + ", doColor: " + FrostyEmote.getInstance().getConfig().getString("doColor")
                + ", playerColor: " + FrostyEmote.getInstance().getConfig().getString("playerColor")
                + ", rightClickToViewCard: " + FrostyEmote.getInstance().getConfig().getBoolean("rightClickToViewCard")
                + ", localOOCChatRadius: " + FrostyEmote.getInstance().getConfig().getInt("localOOCChatRadius")
                + ", localOOCChatColor: " + FrostyEmote.getInstance().getConfig().getString("localOOCChatColor"));
    }

    public boolean hasBeenAltered() {
        return altered;
    }

}
