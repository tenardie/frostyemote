package oyunbaz.frostyemote.commands;

import oyunbaz.frostyemote.FrostyEmote;
import oyunbaz.frostyemote.StorageManager;
import oyunbaz.frostyemote.data.EphemeralData;
import oyunbaz.frostyemote.objects.CharacterCard;
import oyunbaz.frostyemote.utils.ArgumentParser;
import oyunbaz.frostyemote.utils.UUIDChecker;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

import static org.bukkit.Bukkit.getServer;

//TODO: KAYIT DOSYALARINI DUZELT

public class CardCommand {

    public void showCard(CommandSender sender, String[] args, ArrayList<CharacterCard> cards) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (player.hasPermission("fe.card.show") || player.hasPermission("fe.card.*") || player.hasPermission("fe.default")) {
                for (CharacterCard card : cards) {
                    if (card.getPlayerUUID() != null) {
                        if (card.getPlayerUUID().equals(player.getUniqueId())) {
                            player.sendMessage(ChatColor.BOLD + "" + ChatColor.AQUA + "\n----------\n" + "Kimlik Kartı == " + Bukkit.getOfflinePlayer(card.getPlayerUUID()).getName() + "\n----------\n");
                            player.sendMessage(ChatColor.AQUA + "İsim : " + card.getName());
                            player.sendMessage(ChatColor.AQUA + "Irk : " + card.getRace());
                            player.sendMessage(ChatColor.AQUA + "Yaş : " + card.getAge());
                            player.sendMessage(ChatColor.AQUA + "Cinsiyet : " + card.getGender());
                            player.sendMessage(ChatColor.AQUA + "Din : " + card.getReligion());
                            return;
                        }
                    }
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "Yetersiz izinler : 'fe.card.show'");
            }

        }
    }

    public void showHelpMessage(CommandSender sender) {
        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (player.hasPermission("fe.card.help") || player.hasPermission("fe.card.*") || player.hasPermission("fe.default")) {
                sender.sendMessage(ChatColor.BOLD + "" + ChatColor.AQUA + " == " + "Karakter Komutları" + " == ");
                sender.sendMessage(ChatColor.AQUA + "/karakter - Kimlik kartını göster");
                sender.sendMessage(ChatColor.AQUA + "/karakter (oyuncu) - Başkasının kimlik kartına bak.");
                sender.sendMessage(ChatColor.AQUA + "/karakter isim (isim) - Karakter adını değiştir.");
                sender.sendMessage(ChatColor.AQUA + "/karakter irk (irk) - Irkını değiştir.");
                sender.sendMessage(ChatColor.AQUA + "/karakter yas (yas) - Yaşını ayarla.");
                sender.sendMessage(ChatColor.AQUA + "/karakter cinsiyet (cinsiyet) - Cinsiyetini ayarla");
                sender.sendMessage(ChatColor.AQUA + "/karakter din (din) - Dinini ayarla.");
            }
            else {
                player.sendMessage(ChatColor.RED + "Yetersiz izinler : 'fe.card.help'");
            }
        }

    }

    public void changeName(CommandSender sender, String[] args, ArrayList<CharacterCard> cards) {

        int changeNameCooldown = FrostyEmote.getInstance().getConfig().getInt("changeNameCooldown");

        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (player.hasPermission("fe.card.name") || player.hasPermission("fe.card.*") || player.hasPermission("fe.default")) {
                for (CharacterCard card : cards) {

                        if (card.getPlayerUUID().equals(player.getUniqueId())) {

                            if (!EphemeralData.getInstance().getPlayersOnNameChangeCooldown().contains(player.getUniqueId())) {

                                if (args.length > 1) {
                                    card.setName(ArgumentParser.getInstance().createStringFromFirstArgOnwards(args, 1));
                                    player.sendMessage(ChatColor.GREEN + "İsim ayarlandı!.");

                                    if (changeNameCooldown != 0) {
                                        // cooldown
                                        EphemeralData.getInstance().getPlayersOnNameChangeCooldown().add(player.getUniqueId());

                                        int seconds = changeNameCooldown;
                                        getServer().getScheduler().runTaskLater(FrostyEmote.getInstance(), new Runnable() {
                                            @Override
                                            public void run() {
                                                EphemeralData.getInstance().getPlayersOnNameChangeCooldown().remove(player.getUniqueId());
                                                player.sendMessage(ChatColor.GREEN + "Artik karakter adini ayarlayabilirsin.");
                                            }
                                        }, seconds * 20);
                                    }
                                }
                                else {
                                    player.sendMessage(ChatColor.RED + "Kullanim : /karakter isim (isim)");
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.RED + "Tekrar değiştirmeden önce beklemelisin!!");
                            }
                        }

                }
            }
            else {
                player.sendMessage(ChatColor.RED + "Yetersiz izinler : 'fe.card.name'");
            }

        }
    }

    public void changeRace(CommandSender sender, String[] args, ArrayList<CharacterCard> cards) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (player.hasPermission("fe.card.race") || player.hasPermission("fe.card.*") || player.hasPermission("fe.default")) {
                for (CharacterCard card : cards) {

                    if (card.getPlayerUUID().equals(player.getUniqueId())) {

                        if (args.length > 1) {
                            card.setRace(args[1]);
                            player.sendMessage(ChatColor.GREEN + "Irk ayarlandı!");
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "Kullanim : /karakter irk (ırk)");
                        }
                    }
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "Yetersiz izinler: 'fe.card.race'");
            }

        }
    }

    public void changeSubculture(CommandSender sender, String[] args, ArrayList<CharacterCard> cards) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (player.hasPermission("rp.card.subculture") || player.hasPermission("rp.card.*") || player.hasPermission("rp.default")) {
                for (CharacterCard card : cards) {

                    if (card.getPlayerUUID().equals(player.getUniqueId())) {

                        if (args.length > 1) {
                            card.setSubculture(args[1]);
                            player.sendMessage(ChatColor.GREEN + "-");
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "-");
                        }
                    }
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "-");
            }

        }
    }

    public void changeReligion(CommandSender sender, String[] args, ArrayList<CharacterCard> cards) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (player.hasPermission("fe.card.religion") || player.hasPermission("fe.card.*") || player.hasPermission("fe.default")) {
                for (CharacterCard card : cards) {

                    if (card.getPlayerUUID().equals(player.getUniqueId())) {

                        if (args.length > 1) {
                            card.setReligion(args[1]);
                            player.sendMessage(ChatColor.GREEN + "Din ayarlandı!");
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "Kullanim : /karakter din (din)");
                        }
                    }
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "Yetersiz izinler : 'fe.card.religion'");
            }

        }
    }

    public void changeAge(CommandSender sender, String[] args, ArrayList<CharacterCard> cards) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (player.hasPermission("fe.card.age") || player.hasPermission("fe.card.*") || player.hasPermission("fe.default")) {
                for (CharacterCard card : cards) {

                    if (card.getPlayerUUID().equals(player.getUniqueId())) {

                        if (args.length > 1) {
                            card.setAge(Integer.parseInt(args[1]));
                            player.sendMessage(ChatColor.GREEN + "Yaş Ayarlandı!");
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "Kullanim : /karakter yas (yas)");
                        }
                    }
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "Yetersiz izinler: 'fe.card.age'");
            }

        }
    }

    public void changeGender(CommandSender sender, String[] args, ArrayList<CharacterCard> cards) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (player.hasPermission("fe.card.gender") || player.hasPermission("fe.card.*") || player.hasPermission("fe.default")) {
                for (CharacterCard card : cards) {

                    if (card.getPlayerUUID().equals(player.getUniqueId())) {

                        if (args.length > 1) {
                            card.setGender(args[1]);
                            player.sendMessage(ChatColor.GREEN + "Cinsiyet Ayarlandı!");
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "Kullanim : /karakter cinsiyet (cinsiyet)");
                        }
                    }
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "Yetersiz izinler : 'fe.card.gender'");
            }

        }
    }

    public void showPlayerInfo(CommandSender sender, String[] args, ArrayList<CharacterCard> cards) {
        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (player.hasPermission("fe.card.show.others") || player.hasPermission("fe.card.*") || player.hasPermission("fe.default")) {
                for (CharacterCard card : cards) {
                    if (args.length > 0) {
                        if (card.getPlayerUUID().equals(UUIDChecker.getInstance().findUUIDBasedOnPlayerName(args[0]))) {
                            sender.sendMessage(ChatColor.BOLD + "" + ChatColor.AQUA + "\n----------\n" + "Kimlik Kartı == " + Bukkit.getOfflinePlayer(card.getPlayerUUID()).getName() + "\n----------\n");
                            sender.sendMessage(ChatColor.AQUA + "İsim : " + card.getName());
                            sender.sendMessage(ChatColor.AQUA + "Irk : " + card.getRace());
                            sender.sendMessage(ChatColor.AQUA + "Yaş : " + card.getAge());
                            sender.sendMessage(ChatColor.AQUA + "Cinsiyet : " + card.getGender());
                            sender.sendMessage(ChatColor.AQUA + "Din : " + card.getReligion());
                            return;
                        }
                    }
                }

                player.sendMessage(ChatColor.RED + "Oyuncu bulunamadı!");

            }
            else {
                player.sendMessage(ChatColor.RED + "Yetersiz izinler : 'fe.card.show.others'");
            }

        }

    }

    public boolean forceSave(CommandSender sender) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (player.hasPermission("fe.card.forcesave") || player.hasPermission("fe.admin")) {
                StorageManager.getInstance().saveCardFileNames();
                StorageManager.getInstance().saveCards();
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "Yetersiz izinler: 'fe.card.forcesave'");
                return false;
            }

        }
        return false;
    }

    public boolean forceLoad(CommandSender sender) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (player.hasPermission("fe.card.forceload") || player.hasPermission("fe.admin")) {
                StorageManager.getInstance().loadCards();
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "Yetersiz izinler : 'fe.card.forceload'");
                return false;
            }
        }
        return false;
    }
}
