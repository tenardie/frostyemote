package oyunbaz.frostyemote.commands;

import oyunbaz.frostyemote.utils.ColorChecker;
import oyunbaz.frostyemote.FrostyEmote;
import oyunbaz.frostyemote.Messenger;
import oyunbaz.frostyemote.data.PersistentData;
import oyunbaz.frostyemote.utils.ArgumentParser;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WhisperCommand {

    public void sendQuietMessage(CommandSender sender, String[] args) {

        int whisperChatRadius = FrostyEmote.getInstance().getConfig().getInt("whisperChatRadius");
        String whisperChatColor = FrostyEmote.getInstance().getConfig().getString("whisperChatColor");

        // player check
        if (!(sender instanceof Player)) {
            return;
        }

        Player player = (Player) sender;

        if (player.hasPermission("fe.fisilda") || player.hasPermission("fe.default")) {

            if (args.length > 0) {
                String message = ColorChecker.getInstance().getColorByName(whisperChatColor) + "" + String.format("%s fısıldar : \"%s\"", PersistentData.getInstance().getCard(player.getUniqueId()).getName(), ArgumentParser.getInstance().createStringFromArgs(args));
                Messenger.getInstance().sendRPMessageToPlayersWithinDistance(player, message, whisperChatRadius);
            }
            else {
                player.sendMessage(ChatColor.RED + "Kullanim : /fisilda veya /w");
            }

        }
        else {
            player.sendMessage(ChatColor.RED + "Gerekli izinlere sahip degilsin : 'fe.whisper'");
        }

    }

}
