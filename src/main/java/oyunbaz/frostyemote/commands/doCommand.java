package oyunbaz.frostyemote.commands;

import oyunbaz.frostyemote.FrostyEmote;
import oyunbaz.frostyemote.Messenger;
import oyunbaz.frostyemote.data.PersistentData;
import oyunbaz.frostyemote.utils.ArgumentParser;
import oyunbaz.frostyemote.utils.ColorChecker;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class doCommand {

    public boolean doAction(CommandSender sender, String[] args) {

        int emoteRadius = FrostyEmote.getInstance().getConfig().getInt("doRadius");
        String emoteColor = FrostyEmote.getInstance().getConfig().getString("doColor");
        String playerColor = FrostyEmote.getInstance().getConfig().getString("playerColor");

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("fe.do")  || player.hasPermission("fe.default")) {
                if (args.length > 0) {
                    String message = ArgumentParser.getInstance().createStringFromFirstArgOnwards(args, 0);
                    String characterName = PersistentData.getInstance().getCard(player.getUniqueId()).getName();

                    Messenger.getInstance().sendRPMessageToPlayersWithinDistance(player, ColorChecker.getInstance().getColorByName(emoteColor) + "" + ChatColor.ITALIC  + message + ColorChecker.getInstance().getColorByName(playerColor) + "" + " (( " +characterName + " )) ", emoteRadius);
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "Yetersiz izin : 'fe.emote', 'fe.do'");
                return false;
            }

        }
        return false;
    }

}
