package oyunbaz.frostyemote.commands;

import oyunbaz.frostyemote.FrostyEmote;
import oyunbaz.frostyemote.Messenger;
import oyunbaz.frostyemote.data.EphemeralData;
import oyunbaz.frostyemote.data.PersistentData;
import oyunbaz.frostyemote.utils.ArgumentParser;
import oyunbaz.frostyemote.utils.ColorChecker;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LocalOOCChatCommand {

    public void sendLocalOOCMessage(CommandSender sender, String[] args) {

        int localOOCChatRadius = FrostyEmote.getInstance().getConfig().getInt("localOOCChatRadius");
        String localOOCChatColor = FrostyEmote.getInstance().getConfig().getString("localOOCChatColor");

        // player check
        if (!(sender instanceof Player)) {
            return;
        }

        Player player = (Player) sender;

        if (!(player.hasPermission("fe.lokalOOC") || player.hasPermission("fe.default"))) {
            player.sendMessage(ChatColor.RED + "Gerekli izinlere sahip degilsin : 'fe.bagir'");
        }

        if (args.length == 0) {
            player.sendMessage(ChatColor.RED + "Kullanim : /bagir veya /s");
        }
        
        if (args[0].equalsIgnoreCase("gizle")) {
            addToPlayersWhoHaveHiddenLocalOOCChat(player);
        }
        if (args[0].equalsIgnoreCase("goster")) {
            removeFromPlayersWhoHaveHiddenLocalOOCChat(player);
        }
        
        String message = ColorChecker.getInstance().getColorByName(localOOCChatColor) + "" + String.format("<%s> (( %s ))", PersistentData.getInstance().getCard(player.getUniqueId()).getName(), ArgumentParser.getInstance().createStringFromArgs(args));

        Messenger.getInstance().sendOOCMessageToPlayersWithinDistance(player, message, localOOCChatRadius);
    }

    private void addToPlayersWhoHaveHiddenLocalOOCChat(Player player) {
        if (!EphemeralData.getInstance().getPlayersWhoHaveHiddenLocalOOCChat().contains(player.getUniqueId())) {
            EphemeralData.getInstance().getPlayersWhoHaveHiddenLocalOOCChat().add(player.getUniqueId());
            player.sendMessage(ChatColor.GREEN + "OOC Sohbet Gizlendi!");
        }
        else {
            player.sendMessage(ChatColor.RED + "OOC Sohbet zaten Gizli!");
        }
    }

    private void removeFromPlayersWhoHaveHiddenLocalOOCChat(Player player) {
        if (EphemeralData.getInstance().getPlayersWhoHaveHiddenLocalOOCChat().contains(player.getUniqueId())) {
            EphemeralData.getInstance().getPlayersWhoHaveHiddenLocalOOCChat().remove(player.getUniqueId());
            player.sendMessage(ChatColor.GREEN + "OOC Sohbet Acildi!");
        }
        else {
            player.sendMessage(ChatColor.RED + "OOC Sohbet zaten Acik!");
        }
    }

}
