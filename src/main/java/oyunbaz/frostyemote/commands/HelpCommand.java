package oyunbaz.frostyemote.commands;

import oyunbaz.frostyemote.FrostyEmote;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HelpCommand {

    public void showListOfCommands(CommandSender sender) {

        // player check
        if (!(sender instanceof Player)) {
            return;
        }

        Player player = (Player) sender;

        if (player.hasPermission("fe.fehelp") || player.hasPermission("fe.default")) {

            player.sendMessage(ChatColor.AQUA + " == FrostyEmote by Oyunbaz == " + FrostyEmote.getInstance().getVersion() + " == Komutlar == ");
            player.sendMessage(ChatColor.AQUA + "/fehelp -- Komutları gösterir.");
            player.sendMessage(ChatColor.AQUA + "/karakter yardim -- Kimlik Kartı Komutlarını gösterir.");
            player.sendMessage(ChatColor.AQUA + "/lokal veya /rp -- Lokal Sohbet'e geçmeni sağlar.");
            player.sendMessage(ChatColor.AQUA + "/do - /yap veya /me -- Emote vermeni sağlar..");
            player.sendMessage(ChatColor.AQUA + "/bagir veya /s -- Bağırma komutu");
            player.sendMessage(ChatColor.AQUA + "/fisilda veya /w -- Fısıldama Komutu");
            player.sendMessage(ChatColor.AQUA + "/ooc (mesaj) veya /b (mesaj) -- OOC Sohbet etmeni sağlar.");

        }
        else {
            player.sendMessage(ChatColor.RED + "Su izinlere sahip olman gerek : 'fe.fehelp'");
        }

    }

}
