package oyunbaz.frostyemote.commands;

import oyunbaz.frostyemote.FrostyEmote;
import oyunbaz.frostyemote.Messenger;
import oyunbaz.frostyemote.data.PersistentData;
import oyunbaz.frostyemote.utils.ArgumentParser;
import oyunbaz.frostyemote.utils.ColorChecker;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class YellCommand {

    public void sendLoudMessage(CommandSender sender, String[] args) {

        int yellChatRadius = FrostyEmote.getInstance().getConfig().getInt("yellChatRadius");
        String yellChatColor = FrostyEmote.getInstance().getConfig().getString("yellChatColor");

        // player check
        if (!(sender instanceof Player)) {
            return;
        }

        Player player = (Player) sender;

        if (player.hasPermission("fe.bagir") || player.hasPermission("fe.default")) {

            if (args.length > 0) {
                String message = ColorChecker.getInstance().getColorByName(yellChatColor) + "" + String.format("%s bağırır : \"%s\"", PersistentData.getInstance().getCard(player.getUniqueId()).getName(), ArgumentParser.getInstance().createStringFromArgs(args));

                Messenger.getInstance().sendRPMessageToPlayersWithinDistance(player, message, yellChatRadius);
            }
            else {
                player.sendMessage(ChatColor.RED + "Kullanim : /bagir veya /s");
            }

        }
        else {
            player.sendMessage(ChatColor.RED + "Gerekli izinlere sahip degilsin : 'rp.yell'");
        }

    }

}
