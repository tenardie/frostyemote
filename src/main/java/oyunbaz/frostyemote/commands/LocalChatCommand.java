package oyunbaz.frostyemote.commands;

import oyunbaz.frostyemote.data.EphemeralData;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LocalChatCommand {

    public boolean startChattingInLocalChat(CommandSender sender, String[] args) {

        if (!(sender instanceof Player)) {
            return false;
        }

        Player player = (Player) sender;

        if (!(player.hasPermission("fe.lokal") || player.hasPermission("fe.default"))) {
            player.sendMessage(ChatColor.RED + "Gerekli izinlere sahip degilsin : 'fe.lokal'");
            return false;
        }

        if (args.length != 0) {
            if (args[0].equalsIgnoreCase("gizle")) {
                addToPlayersWhoHaveHiddenLocalChat(player);
                return true;
            }
            if (args[0].equalsIgnoreCase("goster")) {
                removeFromPlayersWhoHaveHiddenLocalChat(player);
                return true;
            }
        }

        addPlayerToLocalChat(player);
        return true;
    }

    public static void addPlayerToLocalChat(Player player) {
        if (!EphemeralData.getInstance().getPlayersSpeakingInLocalChat().contains(player.getUniqueId())) {
            EphemeralData.getInstance().getPlayersSpeakingInLocalChat().add(player.getUniqueId());
            player.sendMessage(ChatColor.GREEN + "Lokal Sohbete gectin.");
        }
        else {
            player.sendMessage(ChatColor.RED + "Zaten lokal sohbettesin!");
        }
    }

    private void addToPlayersWhoHaveHiddenLocalChat(Player player) {
        if (!EphemeralData.getInstance().getPlayersWhoHaveHiddenLocalChat().contains(player.getUniqueId())) {
            EphemeralData.getInstance().getPlayersWhoHaveHiddenLocalChat().add(player.getUniqueId());
            player.sendMessage(ChatColor.GREEN + "Lokal Sohbet gizlendi!");
        }
        else {
            player.sendMessage(ChatColor.RED + "Lokal Sohbet zaten gizli!");
        }
    }

    private void removeFromPlayersWhoHaveHiddenLocalChat(Player player) {
        if (EphemeralData.getInstance().getPlayersWhoHaveHiddenLocalChat().contains(player.getUniqueId())) {
            EphemeralData.getInstance().getPlayersWhoHaveHiddenLocalChat().remove(player.getUniqueId());
            player.sendMessage(ChatColor.GREEN + "Lokal Sohbet acildi!");
        }
        else {
            player.sendMessage(ChatColor.RED + "Lokal Sohbet zaten acik!");
        }
    }

}
