package oyunbaz.frostyemote.commands;

import oyunbaz.frostyemote.Messenger;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RollCommand {

    public boolean rollDice(CommandSender sender, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("fe.roll") || player.hasPermission("fe.dice") || player.hasPermission("fe.default")) {
                if (args.length > 0) {
                    try {
                        int max = Integer.parseInt(args[0]);
                        Messenger.getInstance().sendRPMessageToPlayersWithinDistance(player, ChatColor.AQUA + "" + ChatColor.ITALIC + player.getName() + " isimli oyuncu " + max + "'lık zar üstünden " + rollDice(max) + " attı! ", 25);
                    }
                    catch(Exception ignored) {

                    }
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "Gerekli izinlere sahip degilsin: 'de.roll', 'de.dice'");
            }

        }
        return false;
    }

    private int rollDice(int max) {
        return (int)(Math.random() * max + 1);
    }

}
