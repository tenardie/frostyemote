package oyunbaz.frostyemote.commands;

import oyunbaz.frostyemote.data.EphemeralData;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GlobalChatCommand {

    public boolean startChattingInGlobalChat(CommandSender sender, String[] args) {

        if (!(sender instanceof Player)) {
            return false;
        }

        Player player = (Player) sender;

        if (!(player.hasPermission("fe.global") || player.hasPermission("fe.gooc") || player.hasPermission("fe.default"))) {
            player.sendMessage(ChatColor.RED + "'/rp' yazınız.");
            return false;
        }

        if (args.length != 0) {
            if (args[0].equalsIgnoreCase("gizle")) {
                addToPlayersWhoHaveHiddenGlobalChat(player);
                return true;
            }
            if (args[0].equalsIgnoreCase("goster")) {
                removeFromPlayersWhoHaveHiddenGlobalChat(player);
                return true;
            }
        }

        removePlayerFromLocalChat(player);
        return true;
    }

    private void removePlayerFromLocalChat(Player player) {
        if (EphemeralData.getInstance().getPlayersSpeakingInLocalChat().contains(player.getUniqueId())) {
            EphemeralData.getInstance().getPlayersSpeakingInLocalChat().remove(player.getUniqueId());
            player.sendMessage(ChatColor.GREEN + "Global Sohbete geçtin.");
        }
        else {
            player.sendMessage(ChatColor.RED + "Zaten Global Sohbettesin");
        }
    }

    private void addToPlayersWhoHaveHiddenGlobalChat(Player player) {
        if (!EphemeralData.getInstance().getPlayersWhoHaveHiddenGlobalChat().contains(player.getUniqueId())) {
            EphemeralData.getInstance().getPlayersWhoHaveHiddenGlobalChat().add(player.getUniqueId());
            player.sendMessage(ChatColor.GREEN + "Global Sohbet gizlendi!");
        }
        else {
            player.sendMessage(ChatColor.RED + "Global sohbet zaten gizli!");
        }
    }

    private void removeFromPlayersWhoHaveHiddenGlobalChat(Player player) {
        if (EphemeralData.getInstance().getPlayersWhoHaveHiddenGlobalChat().contains(player.getUniqueId())) {
            EphemeralData.getInstance().getPlayersWhoHaveHiddenGlobalChat().remove(player.getUniqueId());
            player.sendMessage(ChatColor.GREEN + "Global sohbet artık görünür!");
        }
        else {
            player.sendMessage(ChatColor.RED + "Global sohbet zaten görünür durumda!");
        }
    }

}
