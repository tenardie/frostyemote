package oyunbaz.frostyemote.commands;

import oyunbaz.frostyemote.FrostyEmote;
import oyunbaz.frostyemote.Messenger;
import oyunbaz.frostyemote.data.PersistentData;
import oyunbaz.frostyemote.utils.ArgumentParser;
import oyunbaz.frostyemote.utils.ColorChecker;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EmoteCommand {

    public boolean emoteAction(CommandSender sender, String[] args) {

        int emoteRadius = FrostyEmote.getInstance().getConfig().getInt("emoteRadius");
        String emoteColor = FrostyEmote.getInstance().getConfig().getString("emoteColor");
        String playerColor = FrostyEmote.getInstance().getConfig().getString("playerColor");

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("fe.me")  || player.hasPermission("fe.default")) {
                if (args.length > 0) {
                    String message = ArgumentParser.getInstance().createStringFromFirstArgOnwards(args, 0);
                    String characterName = PersistentData.getInstance().getCard(player.getUniqueId()).getName();

                    Messenger.getInstance().sendRPMessageToPlayersWithinDistance(player, ColorChecker.getInstance().getColorByName(playerColor) + "" +  characterName + " " + ColorChecker.getInstance().getColorByName(emoteColor) + "" + ChatColor.ITALIC + message, emoteRadius);
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "Yetersiz izin : 'fe.emote', 'fe.me'");
                return false;
            }

        }
        return false;
    }

}
