package oyunbaz.frostyemote.commands;

import oyunbaz.frostyemote.ConfigManager;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ConfigCommand {

    public boolean handleConfigAccess(CommandSender sender, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage("Sadece Oyuncular bu komutu kullanabilir!");
            return false;
        }

        Player player = (Player) sender;

        if (!(player.hasPermission("fe.config") || player.hasPermission("fe.admin"))) {
            player.sendMessage(ChatColor.RED + "Yetersiz izinler!");
            return false;
        }

        if (args.length < 1) {
            player.sendMessage(ChatColor.RED + "Gecerli komutlar : goster, ayarla");
            return false;
        }

        if (args[0].equalsIgnoreCase("goster")) {
            // no further arguments needed, list config
            ConfigManager.getInstance().sendPlayerConfigList(player);
            return true;
        }

        if (args[0].equalsIgnoreCase("ayarla")) {

            // two more arguments needed
            if (args.length > 2) {

                String option = args[1];
                String value = args[2];

                ConfigManager.getInstance().setConfigOption(option, value, player);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "Kullanim: /feconfig ayarla (secenek) (deger)");
                return false;
            }

        }

        player.sendMessage(ChatColor.RED + "Gecerli komutlar: goster, ayarla");

        return false;
    }

}
