package oyunbaz.frostyemote.objects;

import oyunbaz.frostyemote.utils.UUIDChecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.UUID;

public class CharacterCard {
    private UUID playerUUID = null;
    private String name = "/karakter isim <isim> ile kendine isim ver!!!";
    private String race = "varsayilanIrk";
    private String subculture = "varsayilanKultur";
    private int age = 0;
    private String gender = "varsayilanCinsiyet";
    private String religion = "varsayilanDin";

    public CharacterCard(UUID uuid) {
        playerUUID = uuid;
    }

    // storage constructor
    public CharacterCard() {

    }

    void setPlayerUUID(UUID newUUID) {
        playerUUID = newUUID;
    }

    public UUID getPlayerUUID() {
        return playerUUID;
    }

    public void setName(String newUUID) {
        name = newUUID;
    }

    public String getName() {
        return name;
    }

    public void setRace(String newRace) {
        race = newRace;
    }

    public String getRace() {
        return race;
    }

    public void setSubculture(String newSubculture) {
        subculture = newSubculture;
    }

    public String getSubculture() {
        return subculture;
    }

    public void setAge(int newAge) {
        age = newAge;
    }

    public int getAge() {
        return age;
    }

    public void setGender(String newGender) {
        gender = newGender;
    }

    public String getGender() {
        return gender;
    }

    public void setReligion(String newReligion) {
        religion = newReligion;
    }

    public String getReligion() {
        return religion;
    }

    public boolean save() {
        try {
            File saveFolder = new File("./plugins/FrostyEmote/");
            if (!saveFolder.exists()) {
                saveFolder.mkdir();
            }
            File saveFile = new File("./plugins/FrostyEmote/" + playerUUID + ".txt");
            if (saveFile.createNewFile()) {
                System.out.println( playerUUID + " kullanicisina ait bir profil olusturuldu.");
            } else {
                System.out.println("Zaten " + playerUUID + " kullanicisina ait bir profil var.");
            }

            FileWriter saveWriter = new FileWriter("./plugins/FrostyEmote/" + playerUUID + ".txt");

            // actual saving takes place here
            saveWriter.write(playerUUID.toString() + "\n");
            saveWriter.write(name + "\n");
            saveWriter.write(race + "\n");
            saveWriter.write(subculture + "\n");
            saveWriter.write(age + "\n");
            saveWriter.write(gender + "\n");
            saveWriter.write(religion + "\n");

            saveWriter.close();

            return true;

        } catch (IOException e) {
            System.out.println(playerUUID + " Kullanicisinin profilini olustururken hata olustu.");
            e.printStackTrace();
            return false;
        }
    }

    public boolean load(String filename) {
        try {
            File loadFile = new File("./plugins/FrostyEmote/" + filename);
            Scanner loadReader = new Scanner(loadFile);

            // actual loading
            if (loadReader.hasNextLine()) {
                setPlayerUUID(UUID.fromString(loadReader.nextLine()));
            }
            if (loadReader.hasNextLine()) {
                setName(loadReader.nextLine());
            }
            if (loadReader.hasNextLine()) {
                setRace(loadReader.nextLine());
            }
            if (loadReader.hasNextLine()) {
                setSubculture(loadReader.nextLine());
            }
            if (loadReader.hasNextLine()) {
                setAge(Integer.parseInt(loadReader.nextLine()));
            }
            if (loadReader.hasNextLine()) {
                setGender(loadReader.nextLine());
            }
            if (loadReader.hasNextLine()) {
                setReligion(loadReader.nextLine());
            }

            loadReader.close();
            return true;
        } catch (FileNotFoundException e) {
            System.out.println(filename + " dosyasi yuklenirken bir hata olustu.");
            e.printStackTrace();
            return false;
        }
    }

    public boolean legacyLoad(String filename) {
        try {
            File loadFile = new File("./plugins/FrostyEmote/" + filename);
            Scanner loadReader = new Scanner(loadFile);

            // actual loading
            if (loadReader.hasNextLine()) {
                setPlayerUUID(UUIDChecker.getInstance().findUUIDBasedOnPlayerName(loadReader.nextLine()));
            }
            if (loadReader.hasNextLine()) {
                setName(loadReader.nextLine());
            }
            if (loadReader.hasNextLine()) {
                setRace(loadReader.nextLine());
            }
            if (loadReader.hasNextLine()) {
                setSubculture(loadReader.nextLine());
            }
            if (loadReader.hasNextLine()) {
                setAge(Integer.parseInt(loadReader.nextLine()));
            }
            if (loadReader.hasNextLine()) {
                setGender(loadReader.nextLine());
            }
            if (loadReader.hasNextLine()) {
                setReligion(loadReader.nextLine());
            }

            loadReader.close();
            return true;
        } catch (FileNotFoundException e) {
            System.out.println(filename + " dosyasini yuklerken bir hata olustu.");
            e.printStackTrace();
            return false;
        }
    }

}
