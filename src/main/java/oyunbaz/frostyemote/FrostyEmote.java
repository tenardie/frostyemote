package oyunbaz.frostyemote;

import org.bukkit.event.Event;
import org.bukkit.event.Listener;
import oyunbaz.frostyemote.bstats.Metrics;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import java.io.File;
import java.util.ArrayList;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitScheduler;
import oyunbaz.frostyemote.container.InvProvider;
import oyunbaz.frostyemote.frostymedic.DeathEvent;

public class FrostyEmote extends JavaPlugin {
    public static ArrayList<UUID> injured = new ArrayList<UUID>();
    private static FrostyEmote instance;

    private String version = "v3.0-RELEASE";

    public static FrostyEmote getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        System.out.println("FrostyEmote baslatiliyor...");
        instance = this;

        if (!(new File("./plugins/FrostyEmote/config.yml").exists())) {
            ConfigManager.getInstance().saveConfigDefaults();
        } else {
            if (isVersionMismatched()) {
                ConfigManager.getInstance().handleVersionMismatch();
            }
            reloadConfig();
        }

        if (StorageManager.getInstance().oldSaveFolderPresent()) {
            StorageManager.getInstance().legacyLoadCards();
            StorageManager.getInstance().deleteLegacyFiles(new File("./plugins/FrostyEmote/"));
            StorageManager.getInstance().saveCardFileNames();
            StorageManager.getInstance().saveCards();
        } else {
            StorageManager.getInstance().loadCards();
        }

        EventRegistry.getInstance().registerEvents();
        Listener deathEv = new DeathEvent();
        Bukkit.getPluginManager().registerEvents( deathEv, this );
        Listener InvProvide = new InvProvider();
        Bukkit.getPluginManager().registerEvents( InvProvide, this );


        int pluginId = 11088;
        Metrics metrics = new Metrics(this, pluginId);
        registerEvents();
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (injured.contains(player.getUniqueId())) {
                    }
                }
            }

        }, 0L, 20L);

        System.out.println("***=======================================***");
        System.out.println("\n      FROSTYEMOTE BASARIYLA YUKLENDI   \n");
        System.out.println("***=======================================***");
    }


    @Override
    public void onDisable() {
        System.out.println("FrostyEmote durduruluyor.");
        StorageManager.getInstance().saveCardFileNames();
        StorageManager.getInstance().saveCards();
        if (ConfigManager.getInstance().hasBeenAltered()) {
            saveConfig();
        }
        System.out.println("FrostyEmote Durduruldu.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        return CommandInterpreter.getInstance().interpretCommand(sender, label, args);
    }

    public String getVersion() {
        return version;
    }

    public boolean isVersionMismatched() {
        return !getConfig().getString("version").equalsIgnoreCase(getVersion());
    }

    public void registerEvents() {
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new DeathEvent(), this);
        pm.registerEvents(new InvProvider(), this);
    }


    public void event(Event e){
        Bukkit.getPluginManager().callEvent((Event) e);
    }
}