package oyunbaz.frostyemote;

import oyunbaz.frostyemote.commands.*;
import oyunbaz.frostyemote.data.PersistentData;
import org.bukkit.command.CommandSender;

public class CommandInterpreter {

    private static CommandInterpreter instance;

    private CommandInterpreter() {

    }

    public static CommandInterpreter getInstance() {
        if (instance == null) {
            instance = new CommandInterpreter();
        }
        return instance;
    }

    public boolean interpretCommand(CommandSender sender, String label, String[] args) {

        if (label.equalsIgnoreCase("fehelp")) {
            HelpCommand command = new HelpCommand();
            command.showListOfCommands(sender);
            return true;
        }

        if (label.equalsIgnoreCase("karakter")) {
            CardCommand command = new CardCommand();
            if (args.length == 0) {
                command.showCard(sender, args, PersistentData.getInstance().getCards());
                return true;
            } else {

                if (args[0].equalsIgnoreCase("yardim")) {
                    command.showHelpMessage(sender);
                    return true;
                }

                if (args[0].equalsIgnoreCase("isim")) {
                    command.changeName(sender, args, PersistentData.getInstance().getCards());
                    return true;
                }
                if (args[0].equalsIgnoreCase("irk")) {
                    command.changeRace(sender, args, PersistentData.getInstance().getCards());
                    return true;
                }
                if (args[0].equalsIgnoreCase("din")) {
                    command.changeReligion(sender, args, PersistentData.getInstance().getCards());
                    return true;
                }
                if (args[0].equalsIgnoreCase("yas")) {
                    command.changeAge(sender, args, PersistentData.getInstance().getCards());
                    return true;
                }
                if (args[0].equalsIgnoreCase("cinsiyet")) {
                    command.changeGender(sender, args, PersistentData.getInstance().getCards());
                    return true;
                }

                if (args[0].equalsIgnoreCase("forcesave")) {
                    return command.forceSave(sender);
                }

                if (args[0].equalsIgnoreCase("forceload")) {
                    return command.forceLoad(sender);
                }

                command.showPlayerInfo(sender, args, PersistentData.getInstance().getCards());
                return true;
            }
        }


        if (label.equalsIgnoreCase("lokal") || label.equalsIgnoreCase("rp")) {
            LocalChatCommand command = new LocalChatCommand();
            return command.startChattingInLocalChat(sender, args);
        }

        if (label.equalsIgnoreCase("global") || label.equalsIgnoreCase("gooc")) {
            GlobalChatCommand command = new GlobalChatCommand();
            return command.startChattingInGlobalChat(sender, args);
        }

        if (label.equalsIgnoreCase("me")) {
            EmoteCommand command = new EmoteCommand();
            return command.emoteAction(sender, args);
        }

        if (label.equalsIgnoreCase("bagir")|| label.equalsIgnoreCase("s")) {
            YellCommand command = new YellCommand();
            command.sendLoudMessage(sender, args);
            return true;
        }

        if (label.equalsIgnoreCase("fisilda")|| label.equalsIgnoreCase("f")) {
            WhisperCommand command = new WhisperCommand();
            command.sendQuietMessage(sender, args);
            return true;
        }

        if (label.equalsIgnoreCase("feconfig")) {
            ConfigCommand command = new ConfigCommand();
            command.handleConfigAccess(sender, args);
            return true;
        }

        if (label.equalsIgnoreCase("ooc")|| label.equalsIgnoreCase("b")) {
            LocalOOCChatCommand command = new LocalOOCChatCommand();
            command.sendLocalOOCMessage(sender, args);
            return true;
        }
        if (label.equalsIgnoreCase("do")|| label.equalsIgnoreCase("yap")) {
            doCommand command = new doCommand();
            command.doAction(sender, args);
            return true;
        }
        if (label.equalsIgnoreCase("zar")|| label.equalsIgnoreCase("roll")) {
            RollCommand command = new RollCommand();
            command.rollDice(sender, args);
            return true;
        }
        return false;
    }

}
